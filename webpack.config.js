const VueLoaderPlugin = require('vue-loader/lib/plugin')
const MomentLocalesPlugin = require('moment-locales-webpack-plugin');
var path = require('path')

module.exports= {
    mode:'development',
    watch:true,
    entry: './src/dashboard.js',
    output:{
        path:path.resolve(__dirname,'assets'),
        filename:'index.bundle.js'
    },
    module:{
        rules:[
            {
                test:/\.vue$/,
                exclude: /node_modules/,
                loader:'vue-loader'
            },
            {
                test: /\.js$/,
                loader: 'babel-loader'
            },
            {
                test: /\.css$/,
                use: [
                  'vue-style-loader',
                  'css-loader'
                ]
            },
            {
                test: /\.(jpg|png|gif|jpeg)$/,
                use: {
                  loader: 'url-loader',
                },
              },
        ]
    },
    resolve: {
        alias: {
          'vue$': 'vue/dist/vue.esm.js'
        }
    },
    plugins:[
        new VueLoaderPlugin(),
        // To strip all locales except “en”
        new MomentLocalesPlugin(),
    ]
}