import Vue from 'vue'
import App from './App.vue'
import Router from 'vue-router';
import router from './router';


export const bus = new Vue();

Vue.use(Router);

new Vue({
  el: '#app',
  router,
  components:{
  	'FeedsApp': App
  },
  data(){
    return {
      message: "Here is a test for reactivity in Vue Js again",
      objA: {
        id: "5",
        post_status_id: "10",
        sender_id: "1",
        comment: "checking out the comment again for refreshing",
        status: "1",
        comment_date: "2020-11-11 15:32:21",
        firstname: "Alatise",
        lastname: "Oluwaseun",
        sender_image: ""
      }
    }
  },
  updated(){
    console.log(JSON.stringify(this.objA));
  },
  template: `<FeedsApp 
  v-bind:message="this.message"
  v-bind:objA="this.objA"
  ></FeedsApp>`
})
