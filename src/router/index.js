import Vue from 'vue';
import VueRouter from 'vue-router';
import Dashboard from '../pages/Home.vue';
import About from '../pages/About.vue';

Vue.use(VueRouter);

export default new VueRouter({
  mode:'history',
  base:"localhost/gig/development/vue_dev",
  routes: [
    {
      path: '/',
      component: Dashboard,
    },
    {
      path:'/about',
      component:About
    }
  ]
})